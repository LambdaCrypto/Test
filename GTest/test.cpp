#include "pch.h"
#include "..\Settings.h"
#include "..\stdafx.h"

TEST(CSettings, Save) {
	BOOL Return;
	CSettings Settings;
	TCHAR lpszProfileName[] = _T("Crypto");
	Return = Settings.Save(lpszProfileName);
	EXPECT_TRUE(Return);
}

TEST(CSettings, Load) {
	BOOL Return;
	CSettings Settings;
	TCHAR lpszProfileName[] = _T("Crypto");
	Return = Settings.Load(lpszProfileName);
	EXPECT_TRUE(Return);
}
